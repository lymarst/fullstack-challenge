// Import dependencies
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

// MongoDB URL from the docker-compose file

// const mongoDBServer = `${process.env.MONGODB_URL || '127.0.0.1'}`;
// const dbHost = `mongodb://${mongoDBServer}/mean-docker`;
const dbHost = `mongodb://127.0.0.1:27017/mean-docker`;

// Connect to mongodb
mongoose.connect(dbHost);

/* GET api listing. */
router.get('/', (req, res) => {
    res.send('api works');
});

module.exports = router;
