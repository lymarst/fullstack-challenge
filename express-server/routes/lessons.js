// Import dependencies
const express = require('express');
const router = express.Router();
const Lesson = require('../models/lesson').Lesson;
const Student = require('../models/student').Student;

/* GET all lessons. */
router.get('/lessons', (req, res) => {
    Lesson.find({}).sort({ updatedAt: -1 }).exec((err, lessons) => {
        if (err) res.status(500).send(error)
        res.status(200).json(lessons);
    });
});

/* DELETE one lesson. */
router.delete('/lessons/:id', (req, res) => {
    const lessonId = req.params.id;
    Lesson.remove({ _id: lessonId }, (err) => {
        if (err) res.status(500).send(error);
        // Clear all references to removed lesson
        Student.updateMany({}, {
            "$pull": { "completedCourses": { "course_id": lessonId } }
        }, function (err, obj) {
            res.status(200).json({ deleted: true });
        });
    });
});

/* GET one lesson. */
router.get('/lessons/:id', (req, res) => {
    Lesson.findById(req.params.id, (err, lessons) => {
        if (err) res.status(500).send(error);
        res.status(200).json(lessons);
    });
});

/* Create a lesson. */
router.post('/lessons', (req, res) => {
    const { title, content = "some content" } = req.body;
    let lesson = new Lesson({
        title,
        content,
    });

    lesson.save((error, data) => {
        if (error) res.status(500).send(error);

        res.status(201).json(data);
    });
});

module.exports = router;
