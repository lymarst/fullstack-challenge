// Import dependencies
const express = require('express');
const router = express.Router();
const Student = require('../models/student').Student;
const Lesson = require('../models/lesson').Lesson;

// todo: set in .env
const pageSize = 10;

/* GET api listing. */
router.get('/', (req, res) => {
    res.send('api works');
});

function getUserWithPercentile(user, lessonsCount, studentsCount) {
    const percentileComparedOther = Math.round((studentsCount - user.rank) / (studentsCount) * 100);
    // todo: check possibility to add one course twice
    const percentileCompletedLessons =
        lessonsCount === 0
            ? 0
            :
            (user.completedCoursesCount > lessonsCount
                ? lessonsCount
                : user.completedCoursesCount
                / lessonsCount * 100).toFixed(2);
    return {
        ...user,
        percentileCompletedLessons,
        percentileComparedOther,
    };
}

/* GET all students. */
router.get('/students', async (req, res) => {
    const studentsCount = await Student.count({});
    const lessonsCount = await Lesson.count({});
    const { page = 1 } = req.query;
    Student.aggregate([
        {
            "$project": {
                "name": 1,
                "completedCourses": 1,
                "completedCoursesCount": { "$size": "$completedCourses" },
            },
        },
        {
            "$setWindowFields": {
                "sortBy": { "completedCoursesCount": -1 },
                "output": {
                    "rank": {
                        "$rank": {}
                    }
                }
            }
        },
        { "$limit": pageSize },
        { "$skip": (page - 1) * pageSize },
        {
            "$lookup": {
                "from": "lessons",
                "foreignField": "_id",
                "localField": "completedCourses.course_id",
                "as": "completedCoursesList"
            }
        },
        {
            "$project": {
                "completedCourses": 0,
            },
        }
    ])
        .exec(async (err, students) => {
            if (err) res.status(500).send(err);

            // todo: create compose
            res.status(200).json(
                students.map(
                    (
                        student =>
                            getUserWithPercentile(student, lessonsCount, studentsCount)
                    )
                )
            );
        });
});

/* DELETE one student. */
router.delete('/students/:id', (req, res) => {
    Student.remove({ _id: req.params.id }, (err) => {
        if (err) res.status(500).send(error);
        res.status(200).json({ deleted: true });
    });
});

/* GET one student. */
router.get('/students/:id', (req, res) => {
    Student.findById(req.params.id, (err, students) => {
        if (err) res.status(500).send(error)
        res.status(200).json(students);
    });
});

/* Create a student. */
router.post('/students', (req, res) => {
    let student = new Student({
        name: req.body.name
    });

    student.save(error => {
        if (error) res.status(500).send(error);

        res.status(201).json({
            message: 'Student created successfully'
        });
    });
});

/* Update a student. */
router.put('/students/:id/lesson/complete', (req, res) => {
    const course = req.body;
    // student id
    const _id = req.params.id;

    Student.findOneAndUpdate(
        { _id },
        {
            $push: { completedCourses: { course_id: course.course_id, completed_at: new Date() } },
        },
        {
            returnOriginal: true,
            rawResult: true,
            new: true,
        },
        (error, student) => {
            if (error) res.status(500).send(error);

            res.status(200).json(student.value);
        }
    );
});

module.exports = router;
