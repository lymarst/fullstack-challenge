const mongoose = require('mongoose');

// create mongoose schemas
const lessonSchema = new mongoose.Schema({
    title: String,
    content: String
}, { timestamps: true });

// create mongoose model
const Lesson = mongoose.model('Lesson', lessonSchema);

exports.Lesson = Lesson;