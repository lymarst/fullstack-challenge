const mongoose = require('mongoose');

// create mongoose schemas

const studentSchema = new mongoose.Schema({
    name: String,
    completedCourses: [{
        course_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Lesson',
        },
        completed_at: Date
    }],
}, { timestamps: true, collection: 'students' });


// create mongoose model
const Student = mongoose.model('Student', studentSchema);

exports.Student = Student;