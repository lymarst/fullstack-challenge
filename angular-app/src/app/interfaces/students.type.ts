import { LessonInterface } from "./lesson.type";

export interface StudentInterface {
    _id?: string;
    name: string;
    completedCoursesList: LessonInterface[];
    completedCoursesCount: number;
    percentileComparedOther: number;
    percentileCompletedLessons: string;
    rank: number;
}
