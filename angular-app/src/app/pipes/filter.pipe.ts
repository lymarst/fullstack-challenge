import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'customfilter'
})
export class CustomFilterPipe implements PipeTransform {
    transform(
        data: any[],
        listForExclude: Object[],
        fieldForExclude: string,
        fieldByWhichExcludeChoosed: string = '_id',
        fieldByWhichExclude: string = '_id'): any {
        if (!data.length) return data;
        const itemsList = listForExclude[fieldForExclude] || [];
        const excludeItemsIds = itemsList
            .reduce((res, el) => {
                if (el[fieldByWhichExcludeChoosed]) res[el[fieldByWhichExcludeChoosed]] = el;
                return res;
            }, {});

        return data.filter(item => {
            return !excludeItemsIds[item[fieldByWhichExclude]];
        });
    }
}