import { LessonInterface } from './../interfaces/lesson.type';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, retry, shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class LessonsService {
  // todo: replace to .env
  // or create wrapper of HttpClient
  private API: string = 'http://localhost:3000';

  constructor(
    private http: HttpClient,
  ) { }

  public getLessons(page: number = 1): Observable<LessonInterface[]> {
    return this.http.get<LessonInterface[]>(`${this.API}/lessons`)
      .pipe(
        retry(1),
        catchError(this.handleDefaultError<LessonInterface[]>('getLessons', [])),
        shareReplay(),
      );;
  }

  public addLesson(title: string): Observable<LessonInterface> {
    return this.http.post<LessonInterface>(`${this.API}/lessons`, { title })
      .pipe(
        retry(1),
        catchError(this.handleDefaultError<LessonInterface>('addLesson')),
        shareReplay(),
      );;
  }

  public deleteLesson(lesson: LessonInterface): Observable<LessonInterface> {
    return this.http.delete<LessonInterface>(`${this.API}/lessons/` + lesson._id)
      .pipe(
        retry(1),
        catchError(this.handleDefaultError<LessonInterface>('deleteLesson')),
        shareReplay(),
      );;
  }

  private handleDefaultError<T>(method = 'method', result: T = null) {
    return (error: any): Observable<T> => {
      console.log(`[${this.constructor.name} -> ${method}] failed: ${error.message}`);

      return of(result as T);
    };
  }
}