import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, retry, shareReplay } from 'rxjs/operators';
import { StudentInterface } from '../interfaces/students.type';

@Injectable({
    providedIn: 'root',
})
export class StudentsService {
    // todo: replace to .env
    // or create wrapper of HttpClient
    private API: string = 'http://localhost:3000';

    constructor(
        private http: HttpClient,
    ) { }

    public getStudents(page: number = 1): Observable<StudentInterface[]> {
        return this.http.get<StudentInterface[]>(`${this.API}/students`)
            .pipe(
                retry(1),
                catchError(this.handleDefaultError<StudentInterface[]>('getStudents', [])),
                shareReplay(),
            );;
    }

    public addStudent(name: string): Observable<StudentInterface> {
        return this.http.post<StudentInterface>(`${this.API}/students`, { name })
            .pipe(
                retry(1),
                catchError(this.handleDefaultError<StudentInterface>('addStudent')),
                shareReplay(),
            );;
    }

    public deleteStudents(studentId: number): Observable<StudentInterface> {
        return this.http.delete<StudentInterface>(`${this.API}/students/${studentId}`)
            .pipe(
                retry(1),
                catchError(this.handleDefaultError<StudentInterface>('deleteStudents')),
                shareReplay(),
            );;
    }

    public completeCourse(studentId: string, lessonId: string): Observable<StudentInterface> {
        const body = {
            course_id: lessonId,
        };
        return this.http.put<StudentInterface>(`${this.API}/students/${studentId}/lesson/complete`, body)
            .pipe(
                retry(1),
                catchError(this.handleDefaultError<StudentInterface>('completeCourse')),
                shareReplay(),
            );;
    }

    private handleDefaultError<T>(method = 'method', result: T = null) {
        return (error: Error): Observable<T> => {
            console.log(`[${this.constructor.name} -> ${method}] failed: ${error.message}`);

            return of(result as T);
        };
    }
}