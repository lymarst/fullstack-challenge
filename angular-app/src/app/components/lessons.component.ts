import { LessonInterface } from './../interfaces/lesson.type';
import { LessonsService } from './../services/lessons.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  templateUrl: './lessons.component.html'
})
export class LessonsComponent implements OnInit {
  @Input() data;
  public newLessonModelTitle = '';

  // Declare empty list of students
  lessons: any[] = [];

  constructor(
    private readonly lessonsService: LessonsService,
  ) { }

  // Students
  // Add one student to the API
  public addLesson() {
    // tslint:disable-next-line:triple-equals
    if (this.newLessonModelTitle.trim().length === 0) {
      return alert('Please provide a name for the student');
    }
    const title = this.newLessonModelTitle;

    this.lessonsService.addLesson(title)
      .subscribe((data: LessonInterface) => {
        this.getAllLessons();
        this.newLessonModelTitle = '';
      }, (error: Error) => {
        console.log(error);
      });
  }

  public deleteLesson(lesson) {
    this.lessonsService.deleteLesson(lesson)
      .subscribe((data: LessonInterface) => {
        this.getAllLessons();
      }, (error: Error) => {
        console.log(error);
      });
  }

  // Get all students from the API
  public getAllLessons() {
    this.lessonsService.getLessons()
      .subscribe((lessons: LessonInterface[]) => {
        this.lessons = lessons;
      }, (error: Error) => {
        console.log(error);
      });
  }

  // Validate lesson title
  // Can be extended in future with compose
  // 1) checking on empty string
  public validateNewLessonModelTitle(lessonsTitlte: string): boolean {
    return !lessonsTitlte.trim().length;
  }

  ngOnInit(): void {
    this.getAllLessons();
  }

}
