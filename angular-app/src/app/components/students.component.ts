import { LessonsService } from './../services/lessons.service';
import { StudentsService } from './../services/students.service';
import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StudentInterface } from '../interfaces/students.type';

@Component({
  templateUrl: './students.component.html'
})
export class StudentsComponent implements OnInit {
  @Input() data;
  public newStudentModel = '';

  // Declare empty list of students
  students: any[] = [];
  lessons: any[] = [];

  constructor(
    private readonly http: HttpClient,
    private readonly studentsService: StudentsService,
    private readonly lessonsService: LessonsService,
  ) { }

  // Students
  // Add one student to the API
  public addStudent() {
    // tslint:disable-next-line:triple-equals
    if (this.newStudentModel.trim().length === 0) {
      return alert('Please provide a name for the student');
    }
    const name = this.newStudentModel;
    this.studentsService.addStudent(name)
      .subscribe((data: StudentInterface) => {
        this.getAllStudents();
        this.newStudentModel = '';
      }, (error: any) => {
        console.log(error);
      });
  }

  public deleteStudent(student) {
    this.studentsService.deleteStudents(student._id)
      .subscribe((data: any) => {
        this.getAllStudents();
      }, (error: any) => {
        console.log(error);
      });
  }

  // Get all students from the API
  public getAllStudents() {
    this.studentsService.getStudents()
      .subscribe((students: StudentInterface[]) => {
        this.students = students;
      }, (error: any) => {
        console.log(error);
      });
  }

  // Lessons
  // Get all lessons from the API
  public getAllLessons() {
    this.lessonsService.getLessons()
      .subscribe((lessons: any) => {
        this.lessons = lessons;
      }, (error: any) => {
        console.log(error);
      });
  }

  public complete(lesson, student) {
    this.studentsService.completeCourse(student._id, lesson._id)
      .subscribe(data => {
        this.getAllStudents();
      });

  }

  // Validate Students name
  // Can be extended in future with compose
  // 1) checking on empty string
  public validateNewStudentModelTitle(studentsName: string): boolean {
    return !studentsName.trim().length;
  }

  private loadData() {
    this.getAllStudents();
    this.getAllLessons()
  }

  ngOnInit(): void {
    this.loadData();
    
    this.data.reload.subscribe(event => (this.loadData()));
  }

}
